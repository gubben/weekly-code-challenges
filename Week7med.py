"""
Create a function that returns a list of the square of every number up to a given number, only if the number is even.
Use a list comprehension

"""
N=0
x=0
sqlist = []
def get_squares(N):
    """ This function finds the squares of numbers up to N """
    sqlist = [x**2 for x in range(2,N+1,2)]
    print(sqlist) 


def main():
    N = int(input("Enter the max numbers you'd like to find the squares of: "))
    get_squares(N)
      
if __name__ == '__main__':
    main()
    
